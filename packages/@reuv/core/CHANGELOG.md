# @reuv/core

## 0.0.1

### Patch Changes

- [`16ec42c`](https://github.com/rx-ts/reuv/commit/16ec42c3683d552a890b103a405bb2ef2ad8f813) Thanks [@JounQin](https://github.com/JounQin)! - fix: add main/module fields for compatibility, add exports#types field
